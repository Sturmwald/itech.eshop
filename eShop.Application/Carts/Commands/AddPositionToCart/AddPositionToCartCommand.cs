﻿using eShop.Application.Interfaces.Commands.AddPositionToCart;
using eShop.Application.Interfaces.Commands.Factory;
using eShop.Application.Interfaces.Persistence;

namespace eShop.Application.Carts.Commands.AddPositionToCart
{
    public class AddPositionToCartCommand : IAddPositionToCartCommand
    {
        private readonly ICartRepository _cartRepository;
        private readonly IPositionFactory _positionFactory;
        private readonly ICustomerRepository _customerRepository;
        private readonly IProductRepository _productRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AddPositionToCartCommand(ICartRepository cartRepository, IPositionFactory positionFactory, ICustomerRepository customerRepository, IProductRepository productRepository, IUnitOfWork unitOfWork)
        {
            _cartRepository = cartRepository;
            _positionFactory = positionFactory;
            _customerRepository = customerRepository;
            _productRepository = productRepository;
            _unitOfWork = unitOfWork;
        }

        public void Execute(AddPositionToCartItemModel addPositionToCartItemModel)
        {
            var position = _positionFactory.Create(_customerRepository.Get(addPositionToCartItemModel.CustomerID), _productRepository.Get(addPositionToCartItemModel.ProductID));
            _cartRepository.Add(position);
            _unitOfWork.Save();
        }
    }
}
