﻿using eShop.Domain.Customers;
using eShop.Domain.Products;

namespace eShop.Application.Carts.Commands.AddPositionToCart
{
    public class AddPositionToCartItemModel
    {
        public int CustomerID { get; set; }
        public int ProductID { get; set; }
    }
}
