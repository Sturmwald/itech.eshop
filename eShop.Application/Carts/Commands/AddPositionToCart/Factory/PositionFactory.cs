﻿using eShop.Application.Interfaces.Commands.Factory;
using eShop.Domain.Carts;
using eShop.Domain.Customers;
using eShop.Domain.Products;

namespace eShop.Application.Carts.Commands.AddPositionToCart.Factory
{
    public class PositionFactory : IPositionFactory
    {
        public Position Create(Customer customer, Product product)
        {
            var position = new Position();
            position.Customer = customer;
            position.Product = product;

            return position;
        }
    }
}
