﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;

namespace eShop.Application.Carts.Commands.DeletePositionFromCart
{
    public class DeletePositionFromCartCommand : IDeletePositionFromCartCommand
    {
        private readonly ICartRepository _cartRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DeletePositionFromCartCommand(ICartRepository productRepository, IUnitOfWork unitOfWork)
        {
            _cartRepository = productRepository;
            _unitOfWork = unitOfWork;
        }
        public void Execute(DeletePositionFromCartItemModel deletePositionFromCartItemModel)
        {
            var position = _cartRepository.Get(deletePositionFromCartItemModel.ID);
            _cartRepository.Remove(position);
            _unitOfWork.Save();
        }
    }
}
