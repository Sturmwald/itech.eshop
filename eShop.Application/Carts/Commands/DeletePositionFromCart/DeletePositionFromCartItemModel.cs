﻿namespace eShop.Application.Carts.Commands.DeletePositionFromCart
{
    public class DeletePositionFromCartItemModel
    {
        public int ID { get; set; }
    }
}
