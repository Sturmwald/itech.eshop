﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;

namespace eShop.Application.Carts.Commands.EditPositionInCart
{
    public class EditPositionInCartCommand : IEditPositionInCartCommand
    {
        private readonly ICartRepository _cartRepository;
        private readonly IUnitOfWork _unitOfWork;

        public EditPositionInCartCommand(ICartRepository productRepository, IUnitOfWork unitOfWork)
        {
            _cartRepository = productRepository;
            _unitOfWork = unitOfWork;
        }

        public void Execute(EditPositionInCartItemModel editPositionInCartItemModel)
        {
            var product = _cartRepository.Get(editPositionInCartItemModel.ID);
            product.Quantity = editPositionInCartItemModel.Quantity;
            product.DiscountPercentage = editPositionInCartItemModel.DiscountPercentage;
            _cartRepository.Update(product);
            _unitOfWork.Save();
        }
    }
}
