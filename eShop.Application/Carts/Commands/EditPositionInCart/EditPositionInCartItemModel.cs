﻿namespace eShop.Application.Carts.Commands.EditPositionInCart
{
    public class EditPositionInCartItemModel
    {
        public int ID { get; set; }
        public int Quantity { get; set; }
        public int DiscountPercentage { get; set; }
    }
}
