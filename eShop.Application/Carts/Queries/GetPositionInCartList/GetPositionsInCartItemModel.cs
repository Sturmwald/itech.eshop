﻿using eShop.Domain.Products;

namespace eShop.Application.Carts.Queries.GetPositionInCartList
{
    public class GetPositionsInCartItemModel
    {
        public int ID { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal Price { get; set; }
    }
}
