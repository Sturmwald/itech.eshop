﻿using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using eShop.Domain.Customers;
using System.Collections.Generic;
using System.Linq;

namespace eShop.Application.Carts.Queries.GetPositionInCartList
{
    public class GetPositionsInCartQuery : IGetPositionsInCartQuery
    {
        private readonly ICartRepository _cartRepository;
        private readonly ICustomerRepository _customerRepository;

        public GetPositionsInCartQuery(ICartRepository cartRepository, ICustomerRepository customerRepository)
        {
            _cartRepository = cartRepository;
            _customerRepository = customerRepository;
        }

        public List<GetPositionsInCartItemModel> Execute(int customerID)
        {
            var positions = _cartRepository.GetAll()
                .Where(i => i.Customer == _customerRepository.Get(customerID) && i.Order == null)
                .Select(i => new GetPositionsInCartItemModel()
                {
                    ID = i.ID,
                    Product = i.Product,
                    Quantity = i.Quantity,
                    DiscountPercentage = i.DiscountPercentage,
                    Price = i.Price
                });

            return positions.ToList();
        }
    }
}
