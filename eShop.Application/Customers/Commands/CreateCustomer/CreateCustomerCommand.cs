﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Commands.Factory;
using eShop.Application.Interfaces.Persistence;

namespace eShop.Application.Customers.Commands.CreateCustomer
{
    public class CreateCustomerCommand : ICreateCustomerCommand
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly ICustomerFactory _customerFactory;
        private readonly IUnitOfWork _unitOfWork;

        public CreateCustomerCommand(ICustomerRepository customerRepository,
                                    ICustomerFactory customerFactory,
                                    IUnitOfWork unitOfWork)
        {
            _customerRepository = customerRepository;
            _customerFactory = customerFactory;
            _unitOfWork = unitOfWork;
        }

        public void Execute(CreateCustomerItemModel model)
        {
            var customer = _customerFactory.Create(model.CustomerNumber,
                                                  model.LastName,
                                                  model.FirstName
                                                 );
            _customerRepository.Add(customer);
            _unitOfWork.Save();
        }
    }
}
