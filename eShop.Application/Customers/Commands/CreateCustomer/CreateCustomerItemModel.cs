﻿namespace eShop.Application.Customers.Commands.CreateCustomer
{
    public class CreateCustomerItemModel
    {
        public int CustomerNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}
