﻿using eShop.Application.Interfaces.Commands.Factory;
using eShop.Domain.Customers;

namespace eShop.Application.Customers.Commands.CreateCustomer.Factory
{
    public class CustomerFactory : ICustomerFactory
    {
        public Customer Create(int customerNumber, string lastName, string firstName)
        {
            var customer = new Customer();
            customer.CustomerNumber = customerNumber;
            customer.LastName = lastName;
            customer.FirstName = firstName;

            return customer;
        }
    }
}
