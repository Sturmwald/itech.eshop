﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;

namespace eShop.Application.Customers.Commands.DeleteCustomer
{
    public class DeleteCustomerCommand : IDeleteCustomerCommand
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteCustomerCommand(ICustomerRepository customerRepository,
                                    IUnitOfWork unitOfWork)
        {
            _customerRepository = customerRepository;
            _unitOfWork = unitOfWork;
        }

        public void Execute(DeleteCustomerItemModel deleteCustomerItemModel)
        {
            var customer = _customerRepository.Get(deleteCustomerItemModel.ID);
            _customerRepository.Remove(customer);
            _unitOfWork.Save();
        }
    }
}
