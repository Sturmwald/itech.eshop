﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;

namespace eShop.Application.Customers.Commands.EditCustomer
{
    public class EditCustomerCommand : IEditCustomerCommand
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public EditCustomerCommand(ICustomerRepository customerRepository,
                                   IUnitOfWork unitOfWork)
        {
            _customerRepository = customerRepository;
            _unitOfWork = unitOfWork;
        }

        public void Execute(EditCustomerItemModel editCustomerItemModel)
        {
            var customer = _customerRepository.Get(editCustomerItemModel.ID);
            customer.FirstName = editCustomerItemModel.FirstName;
            customer.LastName = editCustomerItemModel.LastName;
            _customerRepository.Update(customer);
            _unitOfWork.Save();
        }
    }
}
