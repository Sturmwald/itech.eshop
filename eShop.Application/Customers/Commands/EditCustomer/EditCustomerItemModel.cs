﻿namespace eShop.Application.Customers.Commands.EditCustomer
{
    public class EditCustomerItemModel
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}
