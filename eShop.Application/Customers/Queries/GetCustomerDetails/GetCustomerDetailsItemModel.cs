﻿namespace eShop.Application.Customers.Queries.GetCustomerDetails
{
    public class GetCustomerDetailsItemModel
    {
        public int ID { get; set; }
        public int CustomerNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}
