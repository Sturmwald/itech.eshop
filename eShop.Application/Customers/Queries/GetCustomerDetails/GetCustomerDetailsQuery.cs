﻿using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using System.Linq;

namespace eShop.Application.Customers.Queries.GetCustomerDetails
{
    public class GetCustomerDetailsQuery : IGetCustomerDetailQuery
    {
        private readonly ICustomerRepository _customerRepository;

        public GetCustomerDetailsQuery(ICustomerRepository productRepository)
        {
            _customerRepository = productRepository;
        }
        public GetCustomerDetailsItemModel Execute(int id)
        {
            var customer = _customerRepository.GetAll()
                .Where(p => p.ID == id)
                .Select(p => new GetCustomerDetailsItemModel()
                {
                    ID = p.ID,
                    CustomerNumber = p.CustomerNumber,
                    LastName = p.LastName,
                    FirstName = p.FirstName
                }).Single();
            return customer;
        }
    }
}
