﻿namespace eShop.Application.Customers.Queries.GetCustomerList
{
    public class GetCustomerListItemModel
    {
        public int ID { get; set; }
        public int CustomerNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}
