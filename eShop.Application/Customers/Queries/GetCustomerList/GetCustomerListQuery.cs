﻿using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using System.Collections.Generic;
using System.Linq;

namespace eShop.Application.Customers.Queries.GetCustomerList
{
    public class GetCustomerListQuery : IGetCustomerListQuery
    {
        private readonly ICustomerRepository _customerRepository;

        public GetCustomerListQuery(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public List<GetCustomerListItemModel> Execute()
        {
            var customerList = _customerRepository.GetAll().Select(p => new GetCustomerListItemModel()
            {
                ID = p.ID,
                CustomerNumber = p.CustomerNumber,
                LastName = p.LastName,
                FirstName = p.FirstName
            });

            return customerList.ToList();
        }
    }
}
