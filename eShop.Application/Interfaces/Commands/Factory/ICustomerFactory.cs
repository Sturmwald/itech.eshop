﻿using eShop.Domain.Customers;

namespace eShop.Application.Interfaces.Commands.Factory
{
    public interface ICustomerFactory
    {
        Customer Create(int customerNumber, string lastName, string firstName);
    }
}
