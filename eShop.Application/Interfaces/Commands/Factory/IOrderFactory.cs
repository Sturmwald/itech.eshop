﻿using eShop.Domain.Customers;
using eShop.Domain.Orders;

namespace eShop.Application.Interfaces.Commands.Factory
{
    public interface IOrderFactory
    {
        Order Create(Customer customer);
    }
}
