﻿using eShop.Domain.Carts;
using eShop.Domain.Customers;
using eShop.Domain.Products;

namespace eShop.Application.Interfaces.Commands.Factory
{
    public interface IPositionFactory
    {
        Position Create(Customer customer, Product product);
    }
}
