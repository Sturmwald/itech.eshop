﻿using eShop.Domain.Products;

namespace eShop.Application.Interfaces.Commands.Factory
{
    public interface IProductFactory
    {
        Product Create(int itemNumber, string name, string description, decimal price);
    }
}
