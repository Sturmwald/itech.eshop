﻿using eShop.Application.Carts.Commands.AddPositionToCart;

namespace eShop.Application.Interfaces.Commands.AddPositionToCart
{
    public interface IAddPositionToCartCommand
    {
        void Execute(AddPositionToCartItemModel addPositionToCartItemModel);
    }
}
