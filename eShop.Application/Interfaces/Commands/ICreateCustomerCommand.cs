﻿using eShop.Application.Customers.Commands.CreateCustomer;

namespace eShop.Application.Interfaces.Commands
{
    public interface ICreateCustomerCommand
    {
        void Execute(CreateCustomerItemModel createCustomerItemModel);
    }
}
