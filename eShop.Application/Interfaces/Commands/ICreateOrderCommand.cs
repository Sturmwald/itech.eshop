﻿using eShop.Application.Orders.Commands.CreateOrder;

namespace eShop.Application.Interfaces.Commands
{
    public interface ICreateOrderCommand
    {
        void Execute(CreateOrderItemModel createOrderItemModel);
    }
}
