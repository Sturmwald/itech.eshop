﻿using eShop.Application.Products.Commands.CreateProduct;

namespace eShop.Application.Interfaces.Commands
{
    public interface ICreateProductCommand
    {
        void Execute(CreateProductItemModel model);
    }
}
