﻿using eShop.Application.Customers.Commands.DeleteCustomer;

namespace eShop.Application.Interfaces.Commands
{
    public interface IDeleteCustomerCommand
    {
        void Execute(DeleteCustomerItemModel deleteCustomerItemModel);
    }
}
