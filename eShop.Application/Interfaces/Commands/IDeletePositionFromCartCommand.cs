﻿using eShop.Application.Carts.Commands.DeletePositionFromCart;

namespace eShop.Application.Interfaces.Commands
{
    public interface IDeletePositionFromCartCommand
    {
        void Execute(DeletePositionFromCartItemModel deletePositionFromCartItemModel);
    }
}
