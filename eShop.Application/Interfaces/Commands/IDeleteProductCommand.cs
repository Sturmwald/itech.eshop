﻿using eShop.Application.Products.Commands.DeleteProduct;

namespace eShop.Application.Interfaces.Commands
{
    public interface IDeleteProductCommand
    {
        void Execute(DeleteProductItemModel deleteProductItemModel);
    }
}
