﻿using eShop.Application.Customers.Commands.EditCustomer;

namespace eShop.Application.Interfaces.Commands
{
    public interface IEditCustomerCommand
    {
        void Execute(EditCustomerItemModel editCustomerItemModel);
    }
}
