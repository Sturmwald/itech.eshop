﻿using eShop.Application.Carts.Commands.EditPositionInCart;

namespace eShop.Application.Interfaces.Commands
{
    public interface IEditPositionInCartCommand
    {
        void Execute(EditPositionInCartItemModel editPositionInCartItemModel);
    }
}
