﻿using eShop.Application.Products.Commands.EditProduct;

namespace eShop.Application.Interfaces.Commands
{
    public interface IEditProductCommand
    {
        void Execute(EditProductItemModel editProductItemModel);
    }
}
