﻿using eShop.Domain.Carts;

namespace eShop.Application.Interfaces.Persistence
{
    public interface ICartRepository : IRepository<Position>
    {
    }
}
