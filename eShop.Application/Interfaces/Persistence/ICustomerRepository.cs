﻿using eShop.Domain.Customers;

namespace eShop.Application.Interfaces.Persistence
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}
