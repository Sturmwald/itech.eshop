﻿using eShop.Domain.Orders;

namespace eShop.Application.Interfaces.Persistence
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}
