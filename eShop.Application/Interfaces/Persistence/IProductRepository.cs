﻿using eShop.Domain.Products;

namespace eShop.Application.Interfaces.Persistence
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
