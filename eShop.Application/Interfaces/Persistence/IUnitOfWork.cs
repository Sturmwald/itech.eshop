﻿namespace eShop.Application.Interfaces.Persistence
{
    public interface IUnitOfWork
    {
        void Save();
    }
}
