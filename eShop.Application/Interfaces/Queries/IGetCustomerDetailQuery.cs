﻿using eShop.Application.Customers.Queries.GetCustomerDetails;

namespace eShop.Application.Interfaces.Queries
{
    public interface IGetCustomerDetailQuery
    {
        GetCustomerDetailsItemModel Execute(int id);
    }
}
