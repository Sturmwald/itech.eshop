﻿using eShop.Application.Customers.Queries.GetCustomerList;
using System.Collections.Generic;

namespace eShop.Application.Interfaces.Queries
{
    public interface IGetCustomerListQuery
    {
        List<GetCustomerListItemModel> Execute();
    }
}
