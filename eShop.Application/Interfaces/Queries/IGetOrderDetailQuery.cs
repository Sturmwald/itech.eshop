﻿using eShop.Application.Orders.Queries.GetOrderDetail;

namespace eShop.Application.Interfaces.Queries
{
    public interface IGetOrderDetailQuery
    {
        GetOrderDetailItemModel Execute(int id);
    }
}
