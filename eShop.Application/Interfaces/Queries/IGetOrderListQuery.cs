﻿using eShop.Application.Orders.Queries.GetOrderList;
using System.Collections.Generic;

namespace eShop.Application.Interfaces.Queries
{
    public interface IGetOrderListQuery
    {
        List<GetOrderListItemModel> Execute();
    }
}
