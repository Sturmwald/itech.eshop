﻿using eShop.Application.Carts.Queries.GetPositionInCartList;
using eShop.Domain.Customers;
using System.Collections.Generic;

namespace eShop.Application.Interfaces.Queries
{
    public interface IGetPositionsInCartQuery
    {
        List<GetPositionsInCartItemModel> Execute(int customerID);
    }
}
