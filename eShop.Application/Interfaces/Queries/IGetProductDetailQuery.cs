﻿using eShop.Application.Products.Queries.GetProductDetail;

namespace eShop.Application.Interfaces.Queries
{
    public interface IGetProductDetailQuery
    {
        GetProductDetailItemModel Execute(int id);
    }
}
