﻿using eShop.Application.Products.Queries.GetProductList;
using System.Collections.Generic;

namespace eShop.Application.Interfaces.Queries
{
    public interface IGetProductListQuery
    {
        List<GetProductListItemModel> Execute();
    }
}
