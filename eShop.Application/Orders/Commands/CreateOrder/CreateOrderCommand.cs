﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Commands.Factory;
using eShop.Application.Interfaces.Persistence;
using System.Linq;

namespace eShop.Application.Orders.Commands.CreateOrder
{
    public class CreateOrderCommand : ICreateOrderCommand
    {
        private readonly IOrderFactory _orderFactory;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderRepository _orderRepository;
        private readonly ICartRepository _cartRepository;
        private readonly ICustomerRepository _customerRepository;
        public CreateOrderCommand(IOrderRepository orderRepository, IOrderFactory orderFactory, IUnitOfWork unitOfWork, ICartRepository cartRepository, ICustomerRepository customerRepository)
        {
            _orderFactory = orderFactory;
            _orderRepository = orderRepository;
            _unitOfWork = unitOfWork;
            _cartRepository = cartRepository;
            _customerRepository = customerRepository;
        }

        public void Execute(CreateOrderItemModel createOrderItemModel)
        {
            var customer = _customerRepository.Get(createOrderItemModel.CustomerID);
            var order = _orderFactory.Create(customer);

            _cartRepository.GetAll().Where(i => i.Customer == customer).ToList().ForEach(i =>
            {
                i.Order = order;
                _cartRepository.Update(i);
            });
            _orderRepository.Add(order);
            _unitOfWork.Save();

        }
    }
}
