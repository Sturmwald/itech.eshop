﻿using eShop.Domain.Customers;

namespace eShop.Application.Orders.Commands.CreateOrder
{
    public class CreateOrderItemModel
    {
        public int CustomerID { get; set; }
    }
}
