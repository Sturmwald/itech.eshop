﻿using eShop.Application.Interfaces.Commands.Factory;
using eShop.Domain.Customers;
using eShop.Domain.Orders;
using System;

namespace eShop.Application.Orders.Commands.CreateOrder.Factory
{
    public class OrderFactory : IOrderFactory
    {
        public Order Create(Customer customer)
        {
            var order = new Order()
            {
                Customer = customer,
                OrderTime = DateTime.Now
            };
            return order;
        }
    }
}
