﻿using eShop.Domain.Carts;
using eShop.Domain.Customers;
using System;
using System.Collections.Generic;

namespace eShop.Application.Orders.Queries.GetOrderDetail
{
    public class GetOrderDetailItemModel
    {
        public int ID { get; set; }
        public Customer Customer { get; set; }
        public DateTime OrderTime { get; set; }
        public List<Position> Positions { get; set; }
    }
}
