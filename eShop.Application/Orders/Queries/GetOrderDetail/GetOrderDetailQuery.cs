﻿using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using System.Linq;

namespace eShop.Application.Orders.Queries.GetOrderDetail
{
    public class GetOrderDetailQuery : IGetOrderDetailQuery
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ICartRepository _cartRepository;

        public GetOrderDetailQuery(IOrderRepository orderRepository, ICartRepository cartRepository)
        {
            _orderRepository = orderRepository;
            _cartRepository = cartRepository;
        }

        public GetOrderDetailItemModel Execute(int id)
        {
            var product = _orderRepository.GetAll()
                .Where(p => p.ID == id)
                .Select(p => new GetOrderDetailItemModel()
                {
                    ID = p.ID,
                    Customer = p.Customer,
                    OrderTime = p.OrderTime,
                    Positions = _cartRepository.GetAll().Where(i => i.Order == p).ToList()
                }).Single();
            return product;
        }
    }
}
