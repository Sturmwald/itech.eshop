﻿using eShop.Domain.Customers;
using System;

namespace eShop.Application.Orders.Queries.GetOrderList
{
    public class GetOrderListItemModel
    {
        public int ID { get; set; }
        public Customer Customer { get; set; }
        public DateTime OrderTime { get; set; }
    }
}
