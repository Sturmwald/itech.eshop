﻿using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using System.Collections.Generic;
using System.Linq;

namespace eShop.Application.Orders.Queries.GetOrderList
{
    public class GetOrderListQuery : IGetOrderListQuery
    {
        private readonly IOrderRepository _orderRepository;

        public GetOrderListQuery(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }
        public List<GetOrderListItemModel> Execute()
        {
            var orders = _orderRepository.GetAll().Select(p => new GetOrderListItemModel()
            {
                ID = p.ID,
                Customer = p.Customer,
                OrderTime = p.OrderTime
            });

            return orders.ToList();
        }
    }
}
