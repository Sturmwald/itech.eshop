﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Commands.Factory;
using eShop.Application.Interfaces.Persistence;

namespace eShop.Application.Products.Commands.CreateProduct
{
    public class CreateProductCommand : ICreateProductCommand
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductFactory _productFactory;
        private readonly IUnitOfWork _unitOfWork;

        public CreateProductCommand(IProductRepository productRepository,
                                    IProductFactory productFactory,
                                    IUnitOfWork unitOfWork)
        {
            _productRepository = productRepository;
            _productFactory = productFactory;
            _unitOfWork = unitOfWork;
        }

        public void Execute(CreateProductItemModel model)
        {
            var product = _productFactory.Create(model.ItemNumber,
                                                 model.Name,
                                                 model.Description,
                                                 model.Price);
            _productRepository.Add(product);
            _unitOfWork.Save();
        }
    }
}
