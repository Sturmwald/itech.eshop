﻿namespace eShop.Application.Products.Commands.CreateProduct
{
    public class CreateProductItemModel
    {
        public int ItemNumber { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
