﻿using eShop.Application.Interfaces.Commands.Factory;
using eShop.Domain.Products;

namespace eShop.Application.Products.Commands.CreateProduct.Factory
{
    public class ProductFactory : IProductFactory
    {
        public Product Create(int itemNumber, string name, string description, decimal price)
        {
            var product = new Product();
            product.ItemNumber = itemNumber;
            product.Name = name;
            product.Description = description;
            product.Price = price;

            return product;
        }
    }
}
