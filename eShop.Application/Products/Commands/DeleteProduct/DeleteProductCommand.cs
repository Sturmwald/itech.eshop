﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;

namespace eShop.Application.Products.Commands.DeleteProduct
{
    public class DeleteProductCommand : IDeleteProductCommand
    {
        private readonly IProductRepository _productRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteProductCommand(IProductRepository productRepository,
                                    IUnitOfWork unitOfWork)
        {
            _productRepository = productRepository;
            _unitOfWork = unitOfWork;
        }

        public void Execute(DeleteProductItemModel model)
        {
            var product = _productRepository.Get(model.ID);
            _productRepository.Remove(product);
            _unitOfWork.Save();
        }
    }
}
