﻿namespace eShop.Application.Products.Commands.DeleteProduct
{
    public class DeleteProductItemModel
    {
        public int ID { get; set; }
    }
}
