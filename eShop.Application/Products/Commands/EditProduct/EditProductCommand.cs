﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;

namespace eShop.Application.Products.Commands.EditProduct
{
    public class EditProductCommand : IEditProductCommand
    {
        private readonly IProductRepository _productRepository;
        private readonly IUnitOfWork _unitOfWork;

        public EditProductCommand(IProductRepository productRepository,
                                    IUnitOfWork unitOfWork)
        {
            _productRepository = productRepository;
            _unitOfWork = unitOfWork;
        }

        public void Execute(EditProductItemModel model)
        {
            var product = _productRepository.Get(model.ID);
            product.Name = model.Name;
            product.Description = model.Description;
            product.Price = model.Price;
            _productRepository.Update(product);
            _unitOfWork.Save();
        }
    }
}
