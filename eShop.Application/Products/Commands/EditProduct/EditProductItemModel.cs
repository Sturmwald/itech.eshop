﻿namespace eShop.Application.Products.Commands.EditProduct
{
    public class EditProductItemModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
