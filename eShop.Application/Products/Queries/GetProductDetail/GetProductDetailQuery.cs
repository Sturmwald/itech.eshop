﻿using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using System.Linq;

namespace eShop.Application.Products.Queries.GetProductDetail
{
    public class GetProductDetailQuery : IGetProductDetailQuery
    {
        private readonly IProductRepository _productRepository;

        public GetProductDetailQuery(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public GetProductDetailItemModel Execute(int id)
        {
            var product = _productRepository.GetAll()
                .Where(p => p.ID == id)
                .Select(p => new GetProductDetailItemModel()
                {
                    ID = p.ID,
                    ItemNumber = p.ItemNumber,
                    Name = p.Name,
                    Description = p.Description,
                    Price = p.Price
                }).Single();
            return product;
        }
    }
}
