﻿namespace eShop.Application.Products.Queries.GetProductList
{
    public class GetProductListItemModel
    {
        public int ID { get; set; }
        public int ItemNumber { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
