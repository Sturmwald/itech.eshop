﻿using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using System.Collections.Generic;
using System.Linq;

namespace eShop.Application.Products.Queries.GetProductList
{
    public class GetProductListQuery : IGetProductListQuery
    {
        private readonly IProductRepository _productRepository;

        public GetProductListQuery(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public List<GetProductListItemModel> Execute()
        {
            var products = _productRepository.GetAll().Select(p => new GetProductListItemModel()
            {
                ID = p.ID,
                ItemNumber = p.ItemNumber,
                Name = p.Name,
                Description = p.Description,
                Price = p.Price
            });

            return products.ToList();
        }
    }
}
