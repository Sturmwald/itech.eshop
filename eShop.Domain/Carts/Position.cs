﻿using eShop.Domain.Common;
using eShop.Domain.Customers;
using eShop.Domain.Orders;
using eShop.Domain.Products;

namespace eShop.Domain.Carts
{
    public class Position : IEntity
    {
        private decimal discountPercentage = 0;
        private int quantity = 1;
        private Product product;

        public int ID { get; set; }

        public Customer Customer { get; set; }
        //public int CustomerID { get; set; }

        public Product Product
        {
            get => product;
            set
            {
                product = value;
                UpdatePrice();
            }
        }
        //public int ProductID { get; set; }
        public Order Order { get; set; }
        //public int? OrderID { get; set; }
        public int Quantity
        {
            get => quantity;
            set
            {
                if (quantity > 0)
                {
                    quantity = value;
                    UpdatePrice();
                }
            }
        }

        public decimal DiscountPercentage
        {
            get => discountPercentage;
            set
            {
                discountPercentage = value;
                UpdatePrice();
            }
        }

        public decimal Price { get; set; }

        private void UpdatePrice()
        {
            Price = (Quantity * Product.Price) * ((100 - DiscountPercentage) / 100);
        }
    }
}
