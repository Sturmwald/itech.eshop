﻿namespace eShop.Domain.Common
{
    public interface IEntity
    {
        int ID { get; set; }
    }
}
