﻿using eShop.Domain.Common;

namespace eShop.Domain.Customers
{
    public class Customer : IEntity
    {
        public int ID { get; set; }
        public int CustomerNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}
