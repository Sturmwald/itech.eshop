﻿using eShop.Domain.Common;
using eShop.Domain.Customers;
using System;

namespace eShop.Domain.Orders
{
    public class Order : IEntity
    {
        public int ID { get; set; }
        public Customer Customer { get; set; }
        //public int CustomerID { get; set; }
        public DateTime OrderTime { get; set; }
    }
}
