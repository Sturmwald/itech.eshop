﻿using eShop.Domain.Common;

namespace eShop.Domain.Products
{
    public class Product : IEntity
    {
        public int ID { get; set; }
        public int ItemNumber { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
