﻿using eShop.Domain.Carts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace eShop.Persistence.Carts
{
    public class CartConfiguration
        : IEntityTypeConfiguration<Position>
    {
        //TODO CartConfiguration
        //https://docs.microsoft.com/de-de/ef/core/modeling/data-seeding

        public void Configure(EntityTypeBuilder<Position> builder)
        {
            builder.HasKey(p => p.ID);

            builder.HasOne(p => p.Customer);

            builder.HasOne(p => p.Product);

            builder.Property(p => p.Quantity)
                   .IsRequired();

            builder.Property(p => p.DiscountPercentage)
                   .IsRequired();

            builder.Property(p => p.Price)
                   .IsRequired();

            builder.HasOne(p => p.Order)
                   .WithMany();

            builder.HasData(
                new { ID = 1, CustomerID = 1, ProductID = 1, Quantity = 4, DiscountPercentage = 0m, Price = 0m },
                new { ID = 2, CustomerID = 1, ProductID = 2, Quantity = 3, DiscountPercentage = 0m, Price = 0m },
                new { ID = 3, CustomerID = 1, ProductID = 3, Quantity = 2, DiscountPercentage = 0m, Price = 0m },
                new { ID = 4, CustomerID = 1, ProductID = 4, Quantity = 1, DiscountPercentage = 0m, Price = 0m },
                new { ID = 5, CustomerID = 1, ProductID = 5, Quantity = 1, DiscountPercentage = 0m, Price = 0m },
                new { ID = 6, CustomerID = 2, ProductID = 1, Quantity = 3, DiscountPercentage = 10m, Price = 0m },
                new { ID = 7, CustomerID = 2, ProductID = 1, Quantity = 3, DiscountPercentage = 0m, Price = 0m },
                new { ID = 8, CustomerID = 2, ProductID = 2, Quantity = 3, DiscountPercentage = 20m, Price = 0m },
                new { ID = 9, CustomerID = 2, ProductID = 2, Quantity = 3, DiscountPercentage = 0m, Price = 0m },
                new { ID = 10, CustomerID = 2, ProductID = 3, Quantity = 3, DiscountPercentage = 50m, Price = 0m },

                new { ID = 11, CustomerID = 3, ProductID = 1, Quantity = 3, DiscountPercentage = 10m, Price = 0m, OrderID = 1 },
                new { ID = 12, CustomerID = 3, ProductID = 1, Quantity = 3, DiscountPercentage = 0m, Price = 0m, OrderID = 1 },
                new { ID = 13, CustomerID = 3, ProductID = 2, Quantity = 3, DiscountPercentage = 20m, Price = 0m, OrderID = 1 },
                new { ID = 14, CustomerID = 3, ProductID = 2, Quantity = 3, DiscountPercentage = 0m, Price = 0m, OrderID = 1 },
                new { ID = 15, CustomerID = 3, ProductID = 3, Quantity = 3, DiscountPercentage = 50m, Price = 0m, OrderID = 1 }
                );
        }
    }
}

