﻿using eShop.Application.Interfaces.Persistence;
using eShop.Domain.Carts;
using eShop.Persistence.Shared;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace eShop.Persistence.Carts
{
    public class CartRepository
        : Repository<Position>,
        ICartRepository
    {
        public CartRepository(IDatabaseContext database) : base(database)
        {
        }

        public override Position Get(int id)
        {
            return _database.Positions.Include(p => p.Product).Include(c => c.Customer).Include(o => o.Order).Single(p => p.ID == id);
        }

        public override IQueryable<Position> GetAll()
        {
            return _database.Positions.Include(p => p.Product).Include(c => c.Customer).Include(o => o.Order);
        }
    }
}
