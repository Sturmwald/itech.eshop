﻿using eShop.Domain.Customers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace eShop.Persistence.Customers
{
    public class CustomerConfiguration
        : IEntityTypeConfiguration<Customer>
    {
        //TODO CustomerConfiguration
        //https://docs.microsoft.com/de-de/ef/core/modeling/data-seeding
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasKey(p => p.ID);

            builder.HasAlternateKey(p => p.CustomerNumber);

            builder.Property(p => p.LastName)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(p => p.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            builder.HasData(
                new { ID = 1, CustomerNumber = 20000001, FirstName = "Alfred", LastName = "Hitchcock" },
                new { ID = 2, CustomerNumber = 20000002, FirstName = "Jesus", LastName = "von Nazareth" },
                new { ID = 3, CustomerNumber = 20000003, FirstName = "Pika", LastName = "Chu" },
                new { ID = 4, CustomerNumber = 20000004, FirstName = "Karl", LastName = "Lama" },
                new { ID = 5, CustomerNumber = 20000005, FirstName = "Frederick", LastName = "Schwein" },
                new { ID = 6, CustomerNumber = 20000006, FirstName = "Harry", LastName = "Potter" },
                new { ID = 7, CustomerNumber = 20000007, FirstName = "Ron", LastName = "Weasley" },
                new { ID = 8, CustomerNumber = 20000008, FirstName = "Hermine", LastName = "Granger" },
                new { ID = 9, CustomerNumber = 20000009, FirstName = "Donald", LastName = "Trump" },
                new { ID = 10, CustomerNumber = 20000010, FirstName = "Angela", LastName = "Merkel" }
                );

        }
    }
}
