﻿using eShop.Application.Interfaces.Persistence;
using eShop.Domain.Customers;
using eShop.Persistence.Shared;

namespace eShop.Persistence.Customers
{
    public class CustomerRepository
        : Repository<Customer>,
        ICustomerRepository
    {
        public CustomerRepository(IDatabaseContext database) : base(database)
        {
        }
    }
}
