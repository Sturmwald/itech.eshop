﻿using eShop.Domain.Orders;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace eShop.Persistence.Orders
{
    public class OrderConfiguration
        : IEntityTypeConfiguration<Order>
    {
        //TODO OrderConfiguration
        //https://docs.microsoft.com/de-de/ef/core/modeling/data-seeding
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(p => p.ID);

            builder.HasOne(p => p.Customer);

            builder.Property(p => p.OrderTime)
                   .IsRequired();

            builder.HasData(
                new { ID = 1, CustomerID = 3, OrderTime = DateTime.Now }
                );
        }
    }
}
