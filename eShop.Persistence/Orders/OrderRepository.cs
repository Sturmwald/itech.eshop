﻿using eShop.Application.Interfaces.Persistence;
using eShop.Domain.Orders;
using eShop.Persistence.Shared;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace eShop.Persistence.Orders
{
    public class OrderRepository
        : Repository<Order>,
        IOrderRepository
    {
        public OrderRepository(IDatabaseContext database) : base(database)
        {
        }
        public override Order Get(int id)
        {
            return _database.Orders.Include(c => c.Customer).Single(p => p.ID == id);
        }

        public override IQueryable<Order> GetAll()
        {
            return _database.Orders.Include(c => c.Customer);
        }
    }
}
