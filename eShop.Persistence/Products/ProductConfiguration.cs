﻿using eShop.Domain.Products;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace eShop.Persistence.Products
{
    public class ProductConfiguration
        : IEntityTypeConfiguration<Product>
    {
        //https://docs.microsoft.com/de-de/ef/core/modeling/data-seeding
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(p => p.ID);

            builder.HasAlternateKey(p => p.ItemNumber);

            builder.Property(p => p.ItemNumber)
                   .IsRequired();

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(p => p.Description)
                .IsRequired()
                .HasMaxLength(500);

            builder.Property(p => p.Price)
                .IsRequired();

            builder.HasData(
                new { ID = 1, ItemNumber = 10000000, Name = "Toothbrush", Description = "The toothbrush is an oral hygiene instrument used to clean the teeth, gums, and tongue.", Price = 5m },
                new { ID = 2, ItemNumber = 10000001, Name = "Magnet", Description = "A magnet is a material or object that produces a magnetic field. ", Price = 121.99m },
                new { ID = 3, ItemNumber = 10000002, Name = "Oil Lamp", Description = "An oil lamp is an object used to produce light continuously for a period of time using an oil-based fuel source. ", Price = 24.99m },
                new { ID = 4, ItemNumber = 10000003, Name = "Sailboat", Description = "A sailboat or sailing boat is a boat propelled partly or entirely by sails smaller than a sailing ship.", Price = 9999.99m },
                new { ID = 5, ItemNumber = 10000004, Name = "IPod", Description = "The iPod is a line of portable media players and multi-purpose pocket computers designed and marketed by Apple Inc. ", Price = 199.99m },
                new { ID = 6, ItemNumber = 10000005, Name = "Television", Description = "Television (TV), sometimes shortened to tele or telly, is a telecommunication medium used for transmitting moving images in monochrome (black and white), or in color, and in two or three dimensions and sound. ", Price = 500.00m },
                new { ID = 7, ItemNumber = 10000006, Name = "Hair Tie", Description = "A hair tie (also called a hair elastic, ponytail holder, hair band, or bobble) is an item used to fasten hair, particularly long hair, away from areas such as the face.", Price = 0.25m },
                new { ID = 8, ItemNumber = 10000007, Name = "Deodorant", Description = "A deodorant is a substance applied to the body to prevent or mask (hide) body odor due to bacterial breakdown of perspiration in the armpits, groin, and feet, and in some cases vaginal secretions.", Price = 1.00m },
                new { ID = 9, ItemNumber = 10000008, Name = "Socks", Description = "A sock is an item of clothing worn on the feet and often covering the ankle or some part of the calf. ", Price = 1.11m },
                new { ID = 10, ItemNumber = 10000009, Name = "Tomato", Description = "The tomato is the edible, often red, berry of the plant Solanum lycopersicum, commonly known as a tomato plant.", Price = 0.75m },
                new { ID = 11, ItemNumber = 10000010, Name = "Street Lights", Description = "A street light, light pole, lamppost, street lamp, light standard or lamp standard is a raised source of light on the edge of a road or path.", Price = 428.54m },
                new { ID = 12, ItemNumber = 10000011, Name = "Flowers", Description = "A flower, sometimes known as a bloom or blossom, is the reproductive structure found in flowering plants (plants of the division Magnoliophyta, also called angiosperms).", Price = 19.75m },
                new { ID = 13, ItemNumber = 10000012, Name = "Camera", Description = "A camera is an optical instrument to capture still images or to record moving images, which are stored in a physical medium such as in a digital system or on photographic film.", Price = 279.95m },
                new { ID = 14, ItemNumber = 10000013, Name = "USB Drive", Description = "A USB flash drive is a data storage device that includes flash memory with an integrated USB interface.", Price = 10.10m }
                );

        }
    }
}
