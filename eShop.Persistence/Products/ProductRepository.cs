﻿using eShop.Application.Interfaces.Persistence;
using eShop.Domain.Products;
using eShop.Persistence.Shared;

namespace eShop.Persistence.Products
{
    public class ProductRepository
        : Repository<Product>,
        IProductRepository
    {
        public ProductRepository(IDatabaseContext database) : base(database)
        {
        }
    }
}
