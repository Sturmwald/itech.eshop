﻿using eShop.Domain.Carts;
using eShop.Domain.Common;
using eShop.Domain.Customers;
using eShop.Domain.Orders;
using eShop.Domain.Products;
using eShop.Persistence.Carts;
using eShop.Persistence.Customers;
using eShop.Persistence.Orders;
using eShop.Persistence.Products;
using Microsoft.EntityFrameworkCore;

namespace eShop.Persistence.Shared
{
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Position> Positions { get; set; }

        public DatabaseContext() : base()
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //Lokale Ausführung in Visual Studio 2019
            //optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=eShop;Trusted_Connection=True;");
            //For Azure In App MySQL
            optionsBuilder.UseMySQL( "MYSQLCONNSTR_localdb" );
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration<Customer>(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration<Product>(new ProductConfiguration());
            modelBuilder.ApplyConfiguration<Position>(new CartConfiguration());
            modelBuilder.ApplyConfiguration<Order>(new OrderConfiguration());
        }

        public new DbSet<T> Set<T>() where T : class, IEntity
        {
            return base.Set<T>();
        }

        public void Save()
        {
            this.SaveChanges();
        }

        public void Reset()
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }
    }
}
