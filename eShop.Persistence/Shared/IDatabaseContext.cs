﻿using eShop.Domain.Carts;
using eShop.Domain.Common;
using eShop.Domain.Customers;
using eShop.Domain.Orders;
using eShop.Domain.Products;
using Microsoft.EntityFrameworkCore;

namespace eShop.Persistence.Shared
{
    public interface IDatabaseContext
    {
        DbSet<Customer> Customers { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<Order> Orders { get; set; }
        DbSet<Position> Positions { get; set; }
        DbSet<T> Set<T>() where T : class, IEntity;

        void Save();

        void Reset();
    }
}
