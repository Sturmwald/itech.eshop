﻿using eShop.Application.Interfaces.Persistence;
using eShop.Domain.Common;
using System.Linq;

namespace eShop.Persistence.Shared
{
    public class Repository<T>
        : IRepository<T>
        where T : class, IEntity
    {
        protected readonly IDatabaseContext _database;

        public Repository(IDatabaseContext database)
        {
            _database = database;
        }

        public virtual IQueryable<T> GetAll()
        {
            return _database.Set<T>();
        }

        public virtual T Get(int id)
        {
            return _database.Set<T>().Single(p => p.ID == id);
        }

        public void Add(T entity)
        {
            _database.Set<T>().Add(entity);
        }

        public void Remove(T entity)
        {
            _database.Set<T>().Remove(entity);
        }

        public void Update(T entity)
        {
            _database.Set<T>().Update(entity);
        }

    }
}
