﻿using eShop.Application.Carts.Commands.AddPositionToCart;
using eShop.Application.Carts.Commands.DeletePositionFromCart;
using eShop.Application.Carts.Commands.EditPositionInCart;
using eShop.Application.Carts.Queries.GetPositionInCartList;
using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Commands.AddPositionToCart;
using eShop.Application.Interfaces.Queries;
using eShop.Domain.Customers;
using eShop.Domain.Products;
using eShop.Persistence.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace eShop.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly IDatabaseContext _databaseContext;
        private readonly IAddPositionToCartCommand _addPositionToCartCommand;
        private readonly IDeletePositionFromCartCommand _deletePositionFromCartCommand;
        private readonly IEditPositionInCartCommand _editPositionInCartCommand;
        private readonly IGetPositionsInCartQuery _getPositionsInCartQuery;

        public CartController(IAddPositionToCartCommand addPositionToCartCommand,
                              IDeletePositionFromCartCommand deletePositionFromCartCommand,
                              IEditPositionInCartCommand editPositionInCartCommand,
                              IGetPositionsInCartQuery getPositionsInCartQuery,
                              IDatabaseContext databaseContext)
        {
            _addPositionToCartCommand = addPositionToCartCommand;
            _deletePositionFromCartCommand = deletePositionFromCartCommand;
            _editPositionInCartCommand = editPositionInCartCommand;
            _getPositionsInCartQuery = getPositionsInCartQuery;
            _databaseContext = databaseContext;
        }

        [Route("reset")]
        public void Reset()
        {
            _databaseContext.Reset();
        }

        [Route("detail/{customerID}")]
        public List<GetPositionsInCartItemModel> GetPositionsInCart(int customerID)
        {
            
            return _getPositionsInCartQuery.Execute(customerID);
        }

        [Route("add/{customerID}/{productID}")]
        public void AddPositionToCartCommand(int customerID, int productID)
        {
            var itemModel = new AddPositionToCartItemModel()
            {
                CustomerID = customerID,
                ProductID = productID
            };
            _addPositionToCartCommand.Execute(itemModel);
        }

        [Route("delete/{id}")]
        public void DeletePositionToCartCommand(int id)
        {
            var itemModel = new DeletePositionFromCartItemModel()
            {
                ID = id
            };
            _deletePositionFromCartCommand.Execute(itemModel);
        }

        [Route("edit/{id}/{discount}/{quantity}")]
        public void EditPositionToCartCommand(int id, int discount, int quantity)
        {
            var itemModel = new EditPositionInCartItemModel()
            {
                ID = id,
                DiscountPercentage = discount,
                Quantity = quantity
            };
            _editPositionInCartCommand.Execute(itemModel);
        }
    }
}