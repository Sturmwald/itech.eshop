﻿using eShop.Application.Customers.Commands.CreateCustomer;
using eShop.Application.Customers.Commands.DeleteCustomer;
using eShop.Application.Customers.Commands.EditCustomer;
using eShop.Application.Customers.Queries.GetCustomerDetails;
using eShop.Application.Customers.Queries.GetCustomerList;
using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Queries;
using eShop.Persistence.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace eShop.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IDatabaseContext _databaseContext;
        private readonly ICreateCustomerCommand _createCustomerCommand;
        private readonly IDeleteCustomerCommand _deleteCustomerCommand;
        private readonly IEditCustomerCommand _editCustomerCommand;

        private readonly IGetCustomerDetailQuery _getCustomerDetailQuery;
        private readonly IGetCustomerListQuery _getCustomerListQuery;

        public CustomerController(ICreateCustomerCommand createCustomerCommand,
                                  IDeleteCustomerCommand deleteCustomerCommand,
                                  IEditCustomerCommand editCustomerCommand,
                                  IGetCustomerDetailQuery getCustomerDetailQuery,
                                  IGetCustomerListQuery getCustomerListQuery,
                                  IDatabaseContext databaseContext)
        {
            _createCustomerCommand = createCustomerCommand;
            _deleteCustomerCommand = deleteCustomerCommand;
            _editCustomerCommand = editCustomerCommand;

            _getCustomerDetailQuery = getCustomerDetailQuery;
            _getCustomerListQuery = getCustomerListQuery;

            _databaseContext = databaseContext;
        }

        [Route("reset")]
        public void Reset()
        {
            _databaseContext.Reset();
        }

        [Route("detail/{id}")]
        public GetCustomerDetailsItemModel GetCustomerDetails(int id)
        {
            return _getCustomerDetailQuery.Execute(id);
        }

        [Route("list")]
        public List<GetCustomerListItemModel> GetCustomerList()
        {
            return _getCustomerListQuery.Execute();
        }

        [Route("create/{number}/{firstname}/{lastname}")]
        public void CreateCustomer(int number, string firstname, string lastname)
        {
            var itemModel = new CreateCustomerItemModel()
            {
                CustomerNumber = number,
                FirstName = firstname,
                LastName = lastname
            };

            _createCustomerCommand.Execute(itemModel);
        }

        [Route("edit/{id}/{firstname}/{lastname}")]
        public void EditCustomer(int id, string firstname, string lastname)
        {
            var itemModel = new EditCustomerItemModel()
            {
                ID = id,
                FirstName = firstname,
                LastName = lastname
            };

            _editCustomerCommand.Execute(itemModel);
        }

        [Route("delete/{id}")]
        public void DeleteCustomer(int id)
        {
            var itemModel = new DeleteCustomerItemModel()
            {
                ID = id
            };

            _deleteCustomerCommand.Execute(itemModel);
        }
    }
}