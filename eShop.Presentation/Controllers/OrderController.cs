﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Queries;
using eShop.Application.Orders.Commands.CreateOrder;
using eShop.Application.Orders.Queries.GetOrderDetail;
using eShop.Application.Orders.Queries.GetOrderList;
using eShop.Domain.Customers;
using eShop.Persistence.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace eShop.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IDatabaseContext _databaseContext;
        private readonly ICreateOrderCommand _createOrderCommand;
        private readonly IGetOrderDetailQuery _getOrderDetailQuery;
        private readonly IGetOrderListQuery _getOrderListQuery;

        public OrderController(ICreateOrderCommand createOrderCommand,
                                IGetOrderDetailQuery getOrderDetailQuery,
                                IGetOrderListQuery getOrderListQuery,
                                IDatabaseContext databaseContext)
        {
            _createOrderCommand = createOrderCommand;
            _getOrderDetailQuery = getOrderDetailQuery;
            _getOrderListQuery = getOrderListQuery;
            _databaseContext = databaseContext;
        }

        [Route("reset")]
        public void Reset()
        {
            _databaseContext.Reset();
        }

        [Route("detail/{id}")]
        public GetOrderDetailItemModel GetOrderDetails(int id)
        {
            return _getOrderDetailQuery.Execute(id);
        }

        [Route("list")]
        public List<GetOrderListItemModel> GetOrderList()
        {
            return _getOrderListQuery.Execute();
        }

        [Route("create/{id}")]
        public void CreateOrder(int customerID)
        {
            var itemModel = new CreateOrderItemModel()
            {
                CustomerID = customerID
            };
            _createOrderCommand.Execute(itemModel);
        }
    }
}