﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Queries;
using eShop.Application.Products.Commands.CreateProduct;
using eShop.Application.Products.Commands.DeleteProduct;
using eShop.Application.Products.Commands.EditProduct;
using eShop.Application.Products.Queries.GetProductDetail;
using eShop.Application.Products.Queries.GetProductList;
using eShop.Persistence.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace eShop.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IDatabaseContext _databaseContext;
        private readonly ICreateProductCommand _createProductCommand;
        private readonly IDeleteProductCommand _deleteProductCommand;
        private readonly IEditProductCommand _editProductCommand;
        private readonly IGetProductDetailQuery _getProductDetailQuery;
        private readonly IGetProductListQuery _getProductListQuery;

        public ProductController(ICreateProductCommand createProductCommand,
                                 IDeleteProductCommand deleteProductCommand,
                                 IEditProductCommand editProductCommand,
                                 IGetProductDetailQuery getProductDetailQuery,
                                 IGetProductListQuery getProductListQuery,
                                 IDatabaseContext databaseContext)
        {
            _createProductCommand = createProductCommand;
            _deleteProductCommand = deleteProductCommand;
            _editProductCommand = editProductCommand;
            _getProductDetailQuery = getProductDetailQuery;
            _getProductListQuery = getProductListQuery;
            _databaseContext = databaseContext;
        }

        [Route("reset")]
        public void Reset()
        {
            _databaseContext.Reset();
        }

        [Route("detail/{id}")]
        public GetProductDetailItemModel GetProductDetails(int id)
        {
            return _getProductDetailQuery.Execute(id);
        }

        [Route("list")]
        public List<GetProductListItemModel> GetProductList()
        {
            return _getProductListQuery.Execute();
        }

        [Route("create/{itemNumber}/{name}/{description}/{price}")]
        public void CreateProduct(int itemNumber, string name, string description, decimal price)
        {
            var itemModel = new CreateProductItemModel()
            {
                ItemNumber = itemNumber,
                Name = name,
                Description = description,
                Price = price
            };

            _createProductCommand.Execute(itemModel);
        }

        [Route("edit/{id}/{name}/{description}/{price}")]
        public void EditProduct(int id, string name, string description, decimal price)
        {
            var itemModel = new EditProductItemModel()
            {
                ID = id,
                Name = name,
                Description = description,
                Price = price
            };

            _editProductCommand.Execute(itemModel);
        }

        [Route("delete/{id}")]
        public void DeleteProduct(int id)
        {
            var itemModel = new DeleteProductItemModel()
            {
                ID = id
            };

            _deleteProductCommand.Execute(itemModel);
        }
    }
}
