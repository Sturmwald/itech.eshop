﻿using eShop.Application.Carts.Queries.GetPositionInCartList;
using eShop.Application.Customers.Queries.GetCustomerList;
using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using eShop.Application.Orders.Queries.GetOrderList;
using eShop.Application.Products.Queries.GetProductList;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;

namespace eShop.Presentation.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IGetCustomerListQuery _getCustomerListQuery;
        private readonly IGetOrderListQuery _getOrderListQuery;
        private readonly IGetPositionsInCartQuery _getPositionsInCartQuery;
        private readonly IGetProductDetailQuery _getProductDetailQuery;
        private readonly IGetProductListQuery _getProductListQuery;
        private readonly ICustomerRepository _customerRepository;

        public IndexModel(IGetCustomerListQuery getCustomerListQuery,
                          IGetOrderListQuery getOrderListQuery,
                          IGetPositionsInCartQuery getPositionsInCartQuery,
                          IGetProductDetailQuery getProductDetailQuery,
                          IGetProductListQuery getProductListQuery,
                          ICustomerRepository customerRepository)
        {
            _getCustomerListQuery = getCustomerListQuery;
            _getOrderListQuery = getOrderListQuery;
            _getPositionsInCartQuery = getPositionsInCartQuery;
            _getProductDetailQuery = getProductDetailQuery;
            _getProductListQuery = getProductListQuery;
            _customerRepository = customerRepository;
        }

        public List<GetCustomerListItemModel> Customers { get; set; }
        public List<GetProductListItemModel> Products { get; set; }
        public List<GetOrderListItemModel> Orders { get; set; }
        public List<GetPositionsInCartItemModel> Positions { get; set; }

        public void OnGet()
        {
            Customers = _getCustomerListQuery.Execute();
            Products = _getProductListQuery.Execute();
            Orders = _getOrderListQuery.Execute();
            Positions = _getPositionsInCartQuery.Execute(2);
        }
    }
}