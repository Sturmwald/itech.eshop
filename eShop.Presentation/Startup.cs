using eShop.Application.Carts.Commands.AddPositionToCart;
using eShop.Application.Carts.Commands.AddPositionToCart.Factory;
using eShop.Application.Carts.Commands.DeletePositionFromCart;
using eShop.Application.Carts.Commands.EditPositionInCart;
using eShop.Application.Carts.Queries.GetPositionInCartList;
using eShop.Application.Customers.Commands.CreateCustomer;
using eShop.Application.Customers.Commands.CreateCustomer.Factory;
using eShop.Application.Customers.Commands.DeleteCustomer;
using eShop.Application.Customers.Commands.EditCustomer;
using eShop.Application.Customers.Queries.GetCustomerDetails;
using eShop.Application.Customers.Queries.GetCustomerList;
using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Commands.AddPositionToCart;
using eShop.Application.Interfaces.Commands.Factory;
using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using eShop.Application.Orders.Commands.CreateOrder;
using eShop.Application.Orders.Commands.CreateOrder.Factory;
using eShop.Application.Orders.Queries.GetOrderDetail;
using eShop.Application.Orders.Queries.GetOrderList;
using eShop.Application.Products.Commands.CreateProduct;
using eShop.Application.Products.Commands.CreateProduct.Factory;
using eShop.Application.Products.Commands.DeleteProduct;
using eShop.Application.Products.Commands.EditProduct;
using eShop.Application.Products.Queries.GetProductDetail;
using eShop.Application.Products.Queries.GetProductList;
using eShop.Persistence.Carts;
using eShop.Persistence.Customers;
using eShop.Persistence.Orders;
using eShop.Persistence.Products;
using eShop.Persistence.Shared;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace eShop.Presentation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson();
            services.AddMvc()
                .AddNewtonsoftJson();

            services.AddScoped<IDatabaseContext, DatabaseContext>();
            services.AddScoped<ICartRepository, CartRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IGetCustomerDetailQuery, GetCustomerDetailsQuery>();
            services.AddScoped<IGetCustomerListQuery, GetCustomerListQuery>();
            services.AddScoped<IGetOrderDetailQuery, GetOrderDetailQuery>();
            services.AddScoped<IGetOrderListQuery, GetOrderListQuery>();
            services.AddScoped<IGetPositionsInCartQuery, GetPositionsInCartQuery>();
            services.AddScoped<IGetProductDetailQuery, GetProductDetailQuery>();
            services.AddScoped<IGetProductListQuery, GetProductListQuery>();
            services.AddScoped<ICustomerFactory, CustomerFactory>();
            services.AddScoped<IOrderFactory, OrderFactory>();
            services.AddScoped<IPositionFactory, PositionFactory>();
            services.AddScoped<IProductFactory, ProductFactory>();
            services.AddScoped<IAddPositionToCartCommand, AddPositionToCartCommand>();
            services.AddScoped<ICreateCustomerCommand, CreateCustomerCommand>();
            services.AddScoped<ICreateOrderCommand, CreateOrderCommand>();
            services.AddScoped<ICreateProductCommand, CreateProductCommand>();
            services.AddScoped<IDeleteCustomerCommand, DeleteCustomerCommand>();
            services.AddScoped<IDeletePositionFromCartCommand, DeletePositionFromCartCommand>();
            services.AddScoped<IDeleteProductCommand, DeleteProductCommand>();
            services.AddScoped<IEditCustomerCommand, EditCustomerCommand>();
            services.AddScoped<IEditPositionInCartCommand, EditPositionInCartCommand>();
            services.AddScoped<IEditProductCommand, EditProductCommand>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //The default HSTS value is 30 days.You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.

               app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });
        }
    }
}
