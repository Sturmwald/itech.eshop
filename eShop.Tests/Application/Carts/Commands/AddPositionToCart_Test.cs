﻿using eShop.Application.Carts.Commands.AddPositionToCart;
using eShop.Application.Carts.Commands.AddPositionToCart.Factory;
using eShop.Application.Interfaces.Commands.AddPositionToCart;
using eShop.Application.Interfaces.Commands.Factory;
using eShop.Application.Interfaces.Persistence;
using eShop.Persistence.Carts;
using eShop.Persistence.Customers;
using eShop.Persistence.Products;
using eShop.Persistence.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace eShop.Tests.Application.Carts.Commands
{
    [TestClass]
    public class AddPositionToCarts_Test
    {
        private readonly IDatabaseContext _databaseContext;
        private readonly IPositionFactory _positionFactory;
        private readonly ICartRepository _cartRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAddPositionToCartCommand _createCustomerCommand;
        private readonly ICustomerRepository _customerRepository;
        private readonly IProductRepository _productRepository;

        public AddPositionToCarts_Test()
        {
            _databaseContext = new DatabaseContext();
            _positionFactory = new PositionFactory();
            _cartRepository = new CartRepository(_databaseContext);
            _customerRepository = new CustomerRepository(_databaseContext);
            _productRepository = new ProductRepository(_databaseContext);
            _unitOfWork = new UnitOfWork(_databaseContext);
            _createCustomerCommand = new AddPositionToCartCommand(_cartRepository, _positionFactory, _customerRepository, _productRepository, _unitOfWork);
        }

        [TestMethod]
        public void TestExecuteShouldCreateWithValidData()
        {
            _createCustomerCommand.Execute(new AddPositionToCartItemModel()
            {
                CustomerID = 10,
                ProductID = 10
            });
            var result = _cartRepository.GetAll().Where(i => i.Customer == _customerRepository.Get(10) && i.Product == _productRepository.Get(10)).Single();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Customer == _customerRepository.Get(10));
            Assert.IsTrue(result.Product == _productRepository.Get(10));
        }

        [TestMethod]
        public void TestExecuteShouldNotCreateWithInvalidData()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _createCustomerCommand.Execute(new AddPositionToCartItemModel()
            {
                CustomerID = -1,
                ProductID = -1
            }));
        }
    }
}
