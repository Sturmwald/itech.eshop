﻿using eShop.Application.Carts.Commands.DeletePositionFromCart;
using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;
using eShop.Persistence.Carts;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace eShop.Tests.Application.Carts.Commands
{
    [TestClass]
    public class DeletePositionFromCart_Test
    {
        private readonly ICartRepository _cartRepository;
        private readonly IDeletePositionFromCartCommand _editPositionInCartCommand;
        private readonly IUnitOfWork _unitOfWork;

        public DeletePositionFromCart_Test()
        {
            var database = new DatabaseContext();
            _cartRepository = new CartRepository(database);
            _unitOfWork = new UnitOfWork(database);
            _editPositionInCartCommand = new DeletePositionFromCartCommand(_cartRepository, _unitOfWork);
        }

        [TestMethod]
        public void TestExecuteShouldDeleteWithValidData()
        {
            _editPositionInCartCommand.Execute(new DeletePositionFromCartItemModel()
            {
                ID = 10
            });
            var result = _cartRepository.GetAll().Where(p => p.ID == 10).SingleOrDefault();

            Assert.IsNull(result);
        }

        [TestMethod]
        public void TestExecuteShouldNotDeleteWithInvalidData()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _editPositionInCartCommand.Execute(new DeletePositionFromCartItemModel()
            {
                ID = -1
            }));
        }
    }
}
