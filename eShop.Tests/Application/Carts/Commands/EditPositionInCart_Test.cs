﻿using eShop.Application.Carts.Commands.EditPositionInCart;
using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;
using eShop.Persistence.Carts;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace eShop.Tests.Application.Carts.Commands
{
    [TestClass]
    public class EditPositionInCart_Test
    {
        private readonly ICartRepository _cartRepository;
        private readonly IEditPositionInCartCommand _editPositionInCartCommand;
        private readonly IUnitOfWork _unitOfWork;

        public EditPositionInCart_Test()
        {
            var database = new DatabaseContext();
            _cartRepository = new CartRepository(database);
            _unitOfWork = new UnitOfWork(database);
            _editPositionInCartCommand = new EditPositionInCartCommand(_cartRepository, _unitOfWork);
        }

        [TestMethod]
        public void TestExecuteShouldEditWithValidData()
        {
            _editPositionInCartCommand.Execute(new EditPositionInCartItemModel()
            {
                ID = 1,
                Quantity = 4,
                DiscountPercentage = 10
            });
            var result = _cartRepository.Get(1);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Quantity == 4);
            Assert.IsTrue(result.DiscountPercentage == 10);
            Assert.IsTrue(result.Price == 18m);
        }

        [TestMethod]
        public void TestExecuteShouldNotEditWithInvalidData()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _editPositionInCartCommand.Execute(new EditPositionInCartItemModel()
            {
                ID = -1,
                Quantity = -1,
                DiscountPercentage = 1000
            })); ;
        }
    }
}
