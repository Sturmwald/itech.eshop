﻿using eShop.Application.Carts.Queries.GetPositionInCartList;
using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using eShop.Persistence.Carts;
using eShop.Persistence.Customers;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace eShop.Tests.Application.Carts.Queries
{
    [TestClass]
    public class GetPositionsInCart_Test
    {
        private readonly ICartRepository _cartRepository;
        private readonly IGetPositionsInCartQuery _getPositionsInCartQuery;
        private readonly ICustomerRepository _customerRepository;

        public GetPositionsInCart_Test()
        {
            var database = new DatabaseContext();
            _cartRepository = new CartRepository(database);
            _customerRepository = new CustomerRepository(database);
            _getPositionsInCartQuery = new GetPositionsInCartQuery(_cartRepository, _customerRepository);
        }

        [TestMethod]
        public void TestExecuteShouldReturnListOfPositions()
        {
            var result = _getPositionsInCartQuery.Execute(1);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.First());
        }
    }
}
