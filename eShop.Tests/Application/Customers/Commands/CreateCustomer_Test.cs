﻿using eShop.Application.Customers.Commands.CreateCustomer;
using eShop.Application.Customers.Commands.CreateCustomer.Factory;
using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Commands.Factory;
using eShop.Application.Interfaces.Persistence;
using eShop.Persistence.Customers;
using eShop.Persistence.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace eShop.Tests.Application.Customers.Commands
{
    [TestClass]
    public class CreateCustomer_Test
    {
        private readonly IDatabaseContext _databaseContext;
        private readonly ICustomerFactory _customerFactory;
        private readonly ICustomerRepository _customerRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICreateCustomerCommand _createCustomerCommand;

        public CreateCustomer_Test()
        {
            _databaseContext = new DatabaseContext();
            _customerFactory = new CustomerFactory();
            _customerRepository = new CustomerRepository(_databaseContext);
            _unitOfWork = new UnitOfWork(_databaseContext);
            _createCustomerCommand = new CreateCustomerCommand(_customerRepository, _customerFactory, _unitOfWork);
        }

        [TestMethod]
        public void TestExecuteShouldCreateWithValidData()
        {
            _createCustomerCommand.Execute(new CreateCustomerItemModel()
            {
                CustomerNumber = 20000011,
                LastName = "Test1",
                FirstName = "Test2"
            });
            var result = _customerRepository.GetAll().Where(p => p.CustomerNumber == 20000011).Single();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.CustomerNumber == 20000011);
            Assert.IsTrue(result.LastName.Equals("Test1"));
            Assert.IsTrue(result.FirstName.Equals("Test2"));
        }

        [TestMethod]
        public void TestExecuteShouldNotCreateWithInvalidData()
        {
            Assert.ThrowsException<DbUpdateException>(() => _createCustomerCommand.Execute(new CreateCustomerItemModel()
            {
                CustomerNumber = 20000001,
                LastName = "Test1",
                FirstName = "Test2"
            }));
        }
    }
}
