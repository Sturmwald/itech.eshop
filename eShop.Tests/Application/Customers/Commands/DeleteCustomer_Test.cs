﻿using eShop.Application.Customers.Commands.DeleteCustomer;
using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;
using eShop.Persistence.Customers;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace eShop.Tests.Application.Customers.Commands
{
    [TestClass]
    public class DeleteCustomer_Test
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IDeleteCustomerCommand _deleteCustomerCommand;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteCustomer_Test()
        {
            var database = new DatabaseContext();
            _customerRepository = new CustomerRepository(database);
            _unitOfWork = new UnitOfWork(database);
            _deleteCustomerCommand = new DeleteCustomerCommand(_customerRepository, _unitOfWork);
        }

        [TestMethod]
        public void TestExecuteShouldDeleteWithValidData()
        {
            _deleteCustomerCommand.Execute(new DeleteCustomerItemModel()
            {
                ID = 10
            });
            var result = _customerRepository.GetAll().Where(p => p.ID == 10).SingleOrDefault();

            Assert.IsNull(result);
        }

        [TestMethod]
        public void TestExecuteShouldNotDeleteWithInvalidData()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _deleteCustomerCommand.Execute(new DeleteCustomerItemModel()
            {
                ID = -1
            }));
        }
    }
}
