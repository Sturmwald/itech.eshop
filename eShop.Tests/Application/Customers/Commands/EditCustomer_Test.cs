﻿using eShop.Application.Customers.Commands.EditCustomer;
using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;
using eShop.Persistence.Customers;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace eShop.Tests.Application.Customers.Commands
{
    [TestClass]
    public class EditCustomer_Test
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IEditCustomerCommand _editCustomerCommand;
        private readonly IUnitOfWork _unitOfWork;

        public EditCustomer_Test()
        {
            var database = new DatabaseContext();
            _customerRepository = new CustomerRepository(database);
            _unitOfWork = new UnitOfWork(database);
            _editCustomerCommand = new EditCustomerCommand(_customerRepository, _unitOfWork);
        }

        [TestMethod]
        public void TestExecuteShouldEditWithValidData()
        {
            _editCustomerCommand.Execute(new EditCustomerItemModel()
            {
                ID = 1,
                FirstName = "Test1",
                LastName = "Test2"
            });
            var result = _customerRepository.Get(1);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.FirstName.Equals("Test1"));
            Assert.IsTrue(result.LastName.Equals("Test2"));
        }

        [TestMethod]
        public void TestExecuteShouldNotEditWithInvalidData()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _editCustomerCommand.Execute(new EditCustomerItemModel()
            {
                ID = -1,
                FirstName = "Test1",
                LastName = "Test2",
            })); ;
        }
    }
}