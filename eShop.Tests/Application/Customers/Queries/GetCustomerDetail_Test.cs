﻿using eShop.Application.Customers.Queries.GetCustomerDetails;
using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using eShop.Persistence.Customers;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace eShop.Tests.Application.Customers.Queries
{
    [TestClass]
    public class GetCustomerDetail_Test
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IGetCustomerDetailQuery _getCustomerDetailQuery;

        public GetCustomerDetail_Test()
        {
            _customerRepository = new CustomerRepository(new DatabaseContext());
            _getCustomerDetailQuery = new GetCustomerDetailsQuery(_customerRepository);
        }

        [TestMethod]
        public void TestExecuteShouldReturnCorrectResult()
        {
            var result = _getCustomerDetailQuery.Execute(1);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ID == 1);
            Assert.IsTrue(result.CustomerNumber == 20000001);
            Assert.IsTrue(result.FirstName.Equals("Alfred"));
            Assert.IsTrue(result.LastName.Equals("Hitchcock"));
        }

        [TestMethod]
        public void TestExecuteShouldThrowExceptionIfAskedForInvalidProductID()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _getCustomerDetailQuery.Execute(-1));
        }
    }
}
