﻿using eShop.Application.Customers.Queries.GetCustomerList;
using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using eShop.Persistence.Customers;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace eShop.Tests.Application.Customers.Queries
{
    [TestClass]
    public class GetCustomerList_Test
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IGetCustomerListQuery _getCustomerListQuery;

        public GetCustomerList_Test()
        {
            _customerRepository = new CustomerRepository(new DatabaseContext());
            _getCustomerListQuery = new GetCustomerListQuery(_customerRepository);
        }

        [TestMethod]
        public void TestExecuteShouldReturnListOfCustomers()
        {
            var result = _getCustomerListQuery.Execute();
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.First());
        }
    }
}
