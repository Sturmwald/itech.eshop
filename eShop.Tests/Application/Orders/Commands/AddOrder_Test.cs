﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Commands.Factory;
using eShop.Application.Interfaces.Persistence;
using eShop.Application.Orders.Commands.CreateOrder;
using eShop.Application.Orders.Commands.CreateOrder.Factory;
using eShop.Persistence.Carts;
using eShop.Persistence.Customers;
using eShop.Persistence.Orders;
using eShop.Persistence.Products;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace eShop.Tests.Application.Orders.Commands
{
    [TestClass]
    public class AddOrder_Test
    {
        private readonly IDatabaseContext _databaseContext;
        private readonly IOrderFactory _orderFactory;
        private readonly ICartRepository _cartRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICreateOrderCommand _createOrderCommand;
        private readonly ICustomerRepository _customerRepository;
        private readonly IProductRepository _productRepository;

        public AddOrder_Test()
        {
            _databaseContext = new DatabaseContext();
            _orderFactory = new OrderFactory();
            _cartRepository = new CartRepository(_databaseContext);
            _orderRepository = new OrderRepository(_databaseContext);
            _customerRepository = new CustomerRepository(_databaseContext);
            _productRepository = new ProductRepository(_databaseContext);
            _unitOfWork = new UnitOfWork(_databaseContext);
            _createOrderCommand = new CreateOrderCommand(_orderRepository, _orderFactory, _unitOfWork, _cartRepository, _customerRepository);
        }

        [TestMethod]
        public void TestExecuteShouldCreateWithValidData()
        {
            _createOrderCommand.Execute(new CreateOrderItemModel()
            {
                CustomerID = 10
            });
            var result = _orderRepository.GetAll().Where(i => i.Customer == _customerRepository.Get(10)).Single();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Customer == _customerRepository.Get(10));
        }

        [TestMethod]
        public void TestExecuteShouldNotCreateWithInvalidData()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _createOrderCommand.Execute(new CreateOrderItemModel()
            {
                CustomerID = -1
            }));
        }
    }
}
