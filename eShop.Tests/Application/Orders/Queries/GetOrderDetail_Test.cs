﻿using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using eShop.Application.Orders.Queries.GetOrderDetail;
using eShop.Persistence.Carts;
using eShop.Persistence.Customers;
using eShop.Persistence.Orders;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace eShop.Tests.Application.Orders.Queries
{
    [TestClass]
    public class GetOrderDetail_Test
    {
        private readonly IOrderRepository _OrderRepository;
        private readonly ICartRepository _CartRepository;
        private readonly ICustomerRepository _CustomerRepository;
        private readonly IGetOrderDetailQuery _getOrderDetailQuery;

        public GetOrderDetail_Test()
        {
            var database = new DatabaseContext();
            _OrderRepository = new OrderRepository(database);
            _CartRepository = new CartRepository(database);
            _CustomerRepository = new CustomerRepository(database);
            _getOrderDetailQuery = new GetOrderDetailQuery(_OrderRepository, _CartRepository);
        }

        [TestMethod]
        public void TestExecuteShouldReturnCorrectResult()
        {
            var result = _getOrderDetailQuery.Execute(1);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ID == 1);
            Assert.IsTrue(result.Customer == _CustomerRepository.Get(3));
        }

        [TestMethod]
        public void TestExecuteShouldThrowExceptionIfAskedForInvalidOrderID()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _getOrderDetailQuery.Execute(-1));
        }
    }
}
