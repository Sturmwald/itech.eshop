﻿using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using eShop.Application.Orders.Queries.GetOrderList;
using eShop.Persistence.Orders;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace eShop.Tests.Application.Orders.Queries
{
    [TestClass]
    public class GetOrderList_Test
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IGetOrderListQuery _getOrderListQuery;

        public GetOrderList_Test()
        {
            _orderRepository = new OrderRepository(new DatabaseContext());
            _getOrderListQuery = new GetOrderListQuery(_orderRepository);
        }

        [TestMethod]
        public void TestExecuteShouldReturnListOfProducts()
        {
            var result = _getOrderListQuery.Execute();
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.First());
        }
    }
}
