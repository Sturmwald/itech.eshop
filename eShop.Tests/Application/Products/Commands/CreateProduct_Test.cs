﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Commands.Factory;
using eShop.Application.Interfaces.Persistence;
using eShop.Application.Products.Commands.CreateProduct;
using eShop.Application.Products.Commands.CreateProduct.Factory;
using eShop.Persistence.Products;
using eShop.Persistence.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace eShop.Tests.Application.Products.Commands
{
    [TestClass]
    public class CreateProduct_Test
    {
        private readonly IProductRepository _productRepository;
        private readonly ICreateProductCommand _createProductCommand;
        private readonly IProductFactory _productFactory;
        private readonly IUnitOfWork _unitOfWork;

        public CreateProduct_Test()
        {
            var database = new DatabaseContext();
            _productRepository = new ProductRepository(database);
            _productFactory = new ProductFactory();
            _unitOfWork = new UnitOfWork(database);
            _createProductCommand = new CreateProductCommand(_productRepository, _productFactory, _unitOfWork);
        }

        [TestMethod]
        public void TestExecuteShouldCreateWithValidData()
        {
            _createProductCommand.Execute(new CreateProductItemModel()
            {
                ItemNumber = 10000014,
                Name = "Test1",
                Description = "Test2",
                Price = 999.4m
            });
            var result = _productRepository.GetAll().Where(p => p.ItemNumber == 10000014).Single();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.ItemNumber == 10000014);
            Assert.IsTrue(result.Name.Equals("Test1"));
            Assert.IsTrue(result.Description.Equals("Test2"));
            Assert.IsTrue(result.Price == 999.4m);
        }

        [TestMethod]
        public void TestExecuteShouldNotCreateWithInvalidData()
        {
            Assert.ThrowsException<DbUpdateException>(() => _createProductCommand.Execute(new CreateProductItemModel()
            {
                ItemNumber = 10000000,
                Name = "Test1",
                Description = "Test2",
                Price = 999.4m
            }));
        }
    }
}
