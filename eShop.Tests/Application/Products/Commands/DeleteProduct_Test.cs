﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;
using eShop.Application.Products.Commands.DeleteProduct;
using eShop.Persistence.Products;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace eShop.Tests.Application.Products.Commands
{
    [TestClass]
    public class DeleteProduct_Test
    {
        private readonly IProductRepository _productRepository;
        private readonly IDeleteProductCommand _deleteProductCommand;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteProduct_Test()
        {
            var database = new DatabaseContext();
            _productRepository = new ProductRepository(database);
            _unitOfWork = new UnitOfWork(database);
            _deleteProductCommand = new DeleteProductCommand(_productRepository, _unitOfWork);
        }

        [TestMethod]
        public void TestExecuteShouldDeleteWithValidData()
        {
            _deleteProductCommand.Execute(new DeleteProductItemModel()
            {
                ID = 10
            });
            var result = _productRepository.GetAll().Where(p => p.ID == 10).SingleOrDefault();

            Assert.IsNull(result);
        }

        [TestMethod]
        public void TestExecuteShouldNotDeleteWithInvalidData()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _deleteProductCommand.Execute(new DeleteProductItemModel()
            {
                ID = -1
            }));
        }
    }
}
