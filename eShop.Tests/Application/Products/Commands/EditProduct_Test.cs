﻿using eShop.Application.Interfaces.Commands;
using eShop.Application.Interfaces.Persistence;
using eShop.Application.Products.Commands.EditProduct;
using eShop.Persistence.Products;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace eShop.Tests.Application.Products.Commands
{
    [TestClass]
    public class EditProduct_Test
    {
        private readonly IProductRepository _productRepository;
        private readonly IEditProductCommand _editProductCommand;
        private readonly IUnitOfWork _unitOfWork;

        public EditProduct_Test()
        {
            var database = new DatabaseContext();
            _productRepository = new ProductRepository(database);
            _unitOfWork = new UnitOfWork(database);
            _editProductCommand = new EditProductCommand(_productRepository, _unitOfWork);
        }

        [TestMethod]
        public void TestExecuteShouldEditWithValidData()
        {
            _editProductCommand.Execute(new EditProductItemModel()
            {
                ID = 1,
                Name = "Test1",
                Description = "Test2",
                Price = 999.4m
            });
            var result = _productRepository.Get(1);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Name.Equals("Test1"));
            Assert.IsTrue(result.Description.Equals("Test2"));
            Assert.IsTrue(result.Price == 999.4m);
        }

        [TestMethod]
        public void TestExecuteShouldNotEditWithInvalidData()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _editProductCommand.Execute(new EditProductItemModel()
            {
                ID = -1,
                Name = "Test1",
                Description = "Test2",
                Price = 999.4m
            })); ;
        }
    }
}
