﻿using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using eShop.Application.Products.Queries.GetProductDetail;
using eShop.Persistence.Products;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace eShop.Tests.Application.Products.Queries
{
    [TestClass]
    public class GetProductDetail_Test
    {
        private readonly IProductRepository _productRepository;
        private readonly IGetProductDetailQuery _getProductDetailQuery;

        public GetProductDetail_Test()
        {
            _productRepository = new ProductRepository(new DatabaseContext());
            _getProductDetailQuery = new GetProductDetailQuery(_productRepository);
        }

        [TestMethod]
        public void TestExecuteShouldReturnCorrectResult()
        {
            var result = _getProductDetailQuery.Execute(1);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ID == 1);
            Assert.IsTrue(result.ItemNumber == 10000000);
            Assert.IsTrue(result.Name.Equals("Toothbrush"));
            Assert.IsTrue(result.Description.Equals("The toothbrush is an oral hygiene instrument used to clean the teeth, gums, and tongue."));
            Assert.IsTrue(result.Price == 5m);
        }

        [TestMethod]
        public void TestExecuteShouldThrowExceptionIfAskedForInvalidProductID()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _getProductDetailQuery.Execute(-1));
        }
    }
}
