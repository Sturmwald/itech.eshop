﻿using eShop.Application.Interfaces.Persistence;
using eShop.Application.Interfaces.Queries;
using eShop.Application.Products.Queries.GetProductList;
using eShop.Persistence.Products;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace eShop.Tests.Application.Products.Queries
{
    [TestClass]
    public class GetProductList_Test
    {
        private readonly IProductRepository _productRepository;
        private readonly IGetProductListQuery _getProductListQuery;

        public GetProductList_Test()
        {
            _productRepository = new ProductRepository(new DatabaseContext());
            _getProductListQuery = new GetProductListQuery(_productRepository);
        }

        [TestMethod]
        public void TestExecuteShouldReturnListOfProducts()
        {
            var result = _getProductListQuery.Execute();
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.First());
        }
    }
}
