﻿using eShop.Persistence.Carts;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace eShop.Tests.Persistence
{
    [TestClass]
    public class CartRepository_Test
    {
        private readonly IDatabaseContext _databaseContext;

        public CartRepository_Test()
        {
            _databaseContext = new DatabaseContext();
        }

        [TestMethod]
        public void TestConstructorShouldCreateRepository()
        {
            Assert.IsNotNull(new CartRepository(_databaseContext));
        }
    }
}
