﻿using eShop.Persistence.Customers;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace eShop.Tests.Persistence
{
    [TestClass]
    public class CustomerRepository_Test
    {
        private readonly IDatabaseContext _databaseContext;

        public CustomerRepository_Test()
        {
            _databaseContext = new DatabaseContext();
        }

        [TestMethod]
        public void TestConstructorShouldCreateRepository()
        {
            Assert.IsNotNull(new CustomerRepository(_databaseContext));
        }
    }
}
