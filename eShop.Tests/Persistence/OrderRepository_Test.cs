﻿using eShop.Persistence.Orders;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace eShop.Tests.Persistence
{
    [TestClass]
    public class OrderRepository_Test
    {
        private readonly IDatabaseContext _databaseContext;

        public OrderRepository_Test()
        {
            _databaseContext = new DatabaseContext();
        }

        [TestMethod]
        public void TestConstructorShouldCreateRepository()
        {
            Assert.IsNotNull(new OrderRepository(_databaseContext));
        }
    }
}
