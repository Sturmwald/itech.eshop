﻿using eShop.Persistence.Products;
using eShop.Persistence.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace eShop.Tests.Persistence
{
    [TestClass]
    public class ProductRepository_Test
    {
        private readonly IDatabaseContext _databaseContext;

        public ProductRepository_Test()
        {
            _databaseContext = new DatabaseContext();
        }

        [TestMethod]
        public void TestConstructorShouldCreateRepository()
        {
            Assert.IsNotNull(new ProductRepository(_databaseContext));
        }
    }
}
